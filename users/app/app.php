<?php

/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */
/**
 * Add your routes here
 */
$app->get('/', function () {
    echo $this['view']->render('index');
});

$app->get('/test', function () {
    echo "the test";
});

$app->post('/api/auth', function () use ($app) {
    $username = $app->request->getPost('username', 'alphanum');
    $password = $app->request->getPost('password');
    $user = Users::findFirstByUsername($username);
    echo json_encode(
        [
            'result' => $user && password_verify($password, $user->password)
        ]
    );
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
