<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class UsersMigration_100 extends Migration
{
    /**      
     * Define the table structure      
     *      
     * @return void 
     */
    public function morph()
    {
        $this->morphTable('users', [
            'columns' => [
                new Column('id', [
                    'type' => Column::TYPE_INTEGER,
                    'notNull' => true,
                    'autoIncrement' => true
                ]),
                new Column('username', [
                    'type' => Column::TYPE_VARCHAR,
                    'notNull' => true,
                    'size' => 40
                ]),
                new Column('password', [
                    'type' => Column::TYPE_VARCHAR,
                    'notNull' => true,
                    'size' => 60
                ])
            ],
            'indexes' => [new Index('PRIMARY', ['id'])]
        ]);
    }

    /**      
     * Run the migrations      
     *      * @return void      
     */

    public function up()
    {
        self::$connection->insert(
            'users',
            [
                'admin',
                password_hash('admin', PASSWORD_BCRYPT),
            ],
            [
                'username',
                'password',
            ]
        );
    }

    /**      
     * Reverse the migrations      
     *
     * @return void      
     */
    public function down()
    { }
}
