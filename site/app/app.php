<?php

/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * Add your routes here
 */
$app->get('/', function () {
    echo $this['view']->render('index');
});

$app->post('/site/login', function () use ($app) {
    $username = $app->request->getPost('username', 'alphanum');
    $password = $app->request->getPost('password');

    $cu = curl_init();

    curl_setopt($cu, CURLOPT_URL, "http://users/api/auth");
    curl_setopt($cu, CURLOPT_POST, 1);
    curl_setopt(
        $cu,
        CURLOPT_POSTFIELDS,
        [
            'username' => $username,
            'password' => $password
        ]
    );
    curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($cu);

    curl_close($cu);

    if(json_decode($result)->result){
        echo "успешная авторизация";
    }else{
        echo "неверный логин или пароль";
    }
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
